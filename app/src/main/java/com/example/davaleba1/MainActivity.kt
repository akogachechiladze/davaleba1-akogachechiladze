package com.example.davaleba1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.util.Log.d
import android.widget.Button
import android.widget.TextView

class MainActivity() : AppCompatActivity() {
    constructor(parcel: Parcel) : this() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        val randomNumberGeneratorButton = findViewById<Button>(R. id.randomNumbeGeneratorButton)
        val randomNumberTextView = findViewById<TextView>(R. id.randomNumberTextView)
        val yesOrNoTextView = findViewById<TextView>(R. id.yesOrNoTextView)
        randomNumberGeneratorButton.setOnClickListener {
            val number: Int = randomNumber()
            d("randomNumber" , "This is random number ${evenNumber()}")
            randomNumberTextView.text = number.toString()
            yesOrNoTextView.text = evenNumber()
        }
    }

    private fun randomNumber() = (-100..100).random()

    val yesOrNo: String = evenNumber()
    fun evenNumber():String{
        if (randomNumber()%5 > 0 && randomNumber()>0 ){
            return "yes"
        }else
            return "no"

    }

}
